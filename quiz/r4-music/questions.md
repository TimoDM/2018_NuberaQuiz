<!-- .slide: data-background="img/start.jpg" -->
## Nubera Tech Familie Quiz
### Wie ben ik...?

---
<!-- .slide: data-background="img/no.jpg" -->
## 1. Fragement 1?
When the lady smiles - Golden Earing <!-- .element: class="answer" -->
<audio controls src="audio/01.mp3"></audio>
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/no.jpg" -->
## 2. Fragement 2?
Yellow - Coldplay <!-- .element: class="answer" -->
<audio controls src="audio/02.mp3"></audio>
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/yes.jpg" -->
## 3. Fragement 3?
Alive and Kicking - Simple Minds <!-- .element: class="answer" -->
<audio controls src="audio/03.mp3"></audio>
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/yes.jpg" -->
## 4. Fragement 4?
Paranoid - Black Sabbath <!-- .element: class="answer" -->
<audio controls src="audio/04.mp3"></audio>
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/yes.jpg" -->
## 5. Fragement 5?
Galway Girl - Ed Sheeran <!-- .element: class="answer" -->
<audio controls src="audio/05.mp3"></audio>
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/yes.jpg" -->
## 6. Fragement 6?
Longee - Arsenal <!-- .element: class="answer" -->
<audio controls src="audio/06.mp3"></audio>
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/afgeven.jpg" -->
## Afgeven!
Brown Eyed Girl - Van Morrison <!-- .element: class="answer" -->
<audio controls src="audio/end.mp3"></audio>
Note: This will only appear in the speaker notes window.
