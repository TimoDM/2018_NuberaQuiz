<!-- .slide: data-background="img/start.jpg" -->
## Nubera Tech Familie Quiz
### Gespleten, geweten?!

---
<!-- .slide: data-background="img/1a.jpg" -->
## 1a. Grieken - Stadstaat?
Troje<!-- .element: class="answer" -->
Note: We zoeken de plaats van een van de meest besproken veldslagen uit de oudheid. De slag werd meermaals beschreven en verfilmd. Volgens de overlevering duurde de strijd, geleverd in 1184 voor Christus, zelfs 10 jaar lang. We geven nog enkele namen prijs van vooraanstaande personages: Hector, Agamemnon, Paris en Achilles. Op welke stranden van deze kuststad in Turkije vond deze slag plaats?

---
<!-- .slide: data-background="img/1b.jpg" -->
## 1b. Romeinen - Stadstaat?
Carthago<!-- .element: class="answer" -->
Note: De opgang van het Romeinse rijk verliep niet zonder slag of stoot. Na eerst de Etrusken een lesje geleerd te hebben, kregen de Romeinen het met deze stadstaat aan de stok. De Punische oorlogen waren een logisch gevolg. Zoals iedereen weet werd de Noord Afrikaanse stadstaat volledig verwoest door de Romeinen in 146 v.Chr. Wat is de naam van deze stadstaat, waarvan volgens de overlevering in 218 v.Chr een leger met olifanten de Alpen was overgestoken?

---
<!-- .slide: data-background="img/2a.jpg" -->
## 2a. Chemie - Persoon?
Curie<!-- .element: class="answer" -->
Note: Polonium en radium, zonder deze dame hadden we beide elementen misschien nooit ontdekt. Graag de naam van deze Pools-Franse schei- en natuurkundige, die 2 nobelprijzen mocht ontvangen voor haar werk.

---
<!-- .slide: data-background="img/2b.jpg" -->
## 2b. Fysica - Persoon?
Newton<!-- .element: class="answer" -->
Note: Wie herinnert zich nog hoe je integralen en differentialen berekent? Beide rekenmethodes werden voor het eerst beschreven door de persoon die we zoeken. Misschien ben je geen fan van zijn wiskundige ontdekkingen, maar je kan niet anders dan fan zijn van zijn 3 wetten, die de basis leggen van de klassieke mechanica. Zijn naam is ook de SI eenheid voor kracht. Graag de naam van deze man?

---
<!-- .slide: data-background="img/3a.jpg" -->
## 3a. Politicus - Persoon?
Pim Fortuyn<!-- .element: class="answer" -->
Note: Hoewel zijn naam het niet zou doen vermoeden, kwam deze man onfortuinlijk om het leven. Deze politicus was een veelbesproken politicus in het begin van de jaren 2000 in de Nederlandse. 9 dagen voor de verkiezingen in 2002 werd de man echter neergeschoten op de parking van Media Park in Hilversum. Graag de voor- en achternaam van deze Nederlandse kaalkop.

---
<!-- .slide: data-background="img/3b.jpg" -->
## 3b. Politica - Persoon?
Theresa May<!-- .element: class="answer" -->
Note: We zoeken de volledige naam van de tweede en huidige vrouwelijke premier van het Verenigd Koninkrijk, na Margaret Thatcher.

---
<!-- .slide: data-background="img/4a.jpg" -->
## 4a. Afrika - Land?
Oeganda<!-- .element: class="answer" -->
Note: Welk Afrikaans land gelegen op de evenaar zoeken we hier? De kleuren van de vlag bevatten dezelfde kleuren als de vlag van België.

---
<!-- .slide: data-background="img/4b.jpg" -->
## 4b. Zuid Amerika - Land?
Ecuador<!-- .element: class="answer" -->
Note: Welk Zuid-Amerikaan land gelegen op de evenaar zoeken we hier? De kleuren van de vlag bevatten dezelfde kleuren als de vlag België, maar het zwart is vervangen door blauw.

---
<!-- .slide: data-background="img/5a.jpg" -->
## 5a. Eten - Welk gerecht?
Ossobuco<!-- .element: class="answer" -->
Note: Graag de naam van een Italiaans vleesgerecht dat zijn oorsprong kent in Milaan. De basis bestaat uit kalfsschenkels. De schenkels worden met onder meer uien, wortel en bleekselderij gestoofd. In het traditionele Milanese recept worden geen tomaten verwerkt. Er zijn ook recepten in omloop met tomaat. Deze recepten met tomaat zouden formeel niet "alla milanese" genoemd moeten worden. Het gerecht wordt afgewerkt met gremolata, een evenredig mengsel van peterselie, gehakte knoflook en citroenzeste.

---
<!-- .slide: data-background="img/5b.jpg" -->
## 5b. Drinken - Welke drank?
B52<!-- .element: class="answer" -->
Note: Deze cocktail bestaat uit een koffielikeur, Baileys en Grand Marnier. Wanneer de cocktail correct wordt bereid, zijn de drie dranken niet gemengd waardoor er in het glas drie afzonderlijke lagen te zien zijn. Deze gelaagdheid wordt veroorzaakt door de verschillende soortelijke dichtheid van de afzonderlijke ingrediënten. De naam van de cocktail verwijst naar een type bommenwerper, gebruikt in de Vietnamoorlog, veelal voor het afwerpen van brandbommen. Deze cocktail wordt dan ook vaak brandend geserveerd. Graag de naam van deze cocktail?

---
<!-- .slide: data-background="img/6a.jpg" -->
## 6a. Sport - Welke sport?
Fierljeppen<!-- .element: class="answer" -->
Note: Welke sport zien we hier?

---
<!-- .slide: data-background="img/6b.jpg" -->
## 6b. Spel - Welk spel?
Wipe out!<!-- .element: class="answer" -->
Note: Welk bekend spel zien we hier?

---
<!-- .slide: data-background="img/7a.jpg" -->
## 7a. Oud Testament - Term?
Salomonsoordeel<!-- .element: class="answer" -->
Note: De beschrijving van de term die we zoeken gaat als volgt:
Een xxx is een vonnis of uitspraak in een lastig geschil die getuigt van wijsheid en spitsvondigheid. Geen van de strijdende partijen heeft ogenschijnlijk een voordeel bij deze uitspraak. Uit de reactie van de strijdende partijen leidt de rechter vervolgens af welke partij in haar recht staat. Welke term zoeken we hier?

---
<!-- .slide: data-background="img/7b.jpg" -->
## 7b. Nieuw Testament - Welke rechter?
Pontius Pilatus<!-- .element: class="answer" -->
Note: De man die we zoeken was een Romeins politicus uit de eerste eeuw na Chr. Hij was van 26 - 36 na Chr. de 5e praefectus civitatum van Judea, onder het gezag van het Romeinse Rijk ten tijde van Keizer Tiberius. In het Nieuwe Testament en de Apostolische geloofsbelijdenis wordt hij beschreven als degene die Jezus van Nazareth liet kruisigen. Graag de volledige naam van deze man?

---
<!-- .slide: data-background="img/8a.jpg" -->
## 8a. Jong - Serie?
House of Cards<!-- .element: class="answer" -->
Note: We zoeken een Netflix serie gestart in 2013. Het verhaal beschrijft de avonturen van "Frank" Underwood, een genadeloos en ambitieus politicus in Washington D.C. Het politieke drama is gebaseerd op de gelijknamige Britse miniserie uit 1990. In oktober 2017 werd bekend dat er na het zesde seizoen geen nieuw seizoen meer zou volgen. De acteur die Underwood vertolkt, was inmiddels in opspraak geraakt wegens seksuele intimidatie. Wat is de naam van deze serie?

---
<!-- .slide: data-background="img/8b.jpg" -->
## 8b. Oud - Serie?
Dallas<!-- .element: class="answer" -->
Note: Deze serie is een Amerikaanse soapserie die tussen 2 april 1978 en 3 mei 1991 werd uitgezonden door het Amerikaanse televisiestation CBS. De serie draaide om de familie Ewing, die haar fortuin had gemaakt in de olie-industrie. In totaal werden er 14 seizoenen en 357 afleveringen van de serie geproduceerd. De serie staat daarmee op de 10e plaats van langstlopende series in de Verenigde Staten. Graag de naam van deze serie?

---
<!-- .slide: data-background="img/9a.jpg" -->
## 9a. Zwart - Dier?
Puma<!-- .element: class="answer" -->
Note: Graag de naam van het dier op de foto. Als hint geven we nog de beschrijving van het dier mee. Het dier is stevig gebouwd met sterke poten en een lange, dikke staart die evenwicht geeft bij het achtervolgen van prooidieren. De achterpoten van deze diersoort zijn langer dan de voorpoten. Dit stelt de het dier in staat erg grote sprongen te maken. Uit stand kan het 2,5 meter hoog en bijna 6 meter ver springen. Er zijn sprongen waargenomen van bijna 5 meter hoog en 12 meter ver. De brede poten zorgen voor voldoende grip op oneffen terrein, zoals berggebieden. Net als bij katten hebben de voorpoten vijf tenen en de achterpoten vier. De oren zijn relatief klein.

---
<!-- .slide: data-background="img/9b.jpg" -->
## 9b. Wit - Dier?
Sneeuwluipaard <!-- .element: class="answer" -->
Note: Graag de naam van het dier op de foto. Als hint geven we hier ook de beschrijving van het dier mee. De vacht is lichtgrijs en neigt soms iets naar geel. Hij heeft zwarte vlekken op zijn lijf. De vacht is dik en zijdezacht en de ronde kop is relatief klein. Zijn lichaamslengte ligt tussen 1 en 1,30 meter. Zijn staart is 80 centimeter tot 1 meter en de schofthoogte is circa 60 centimeter.

---
<!-- .slide: data-background="img/10a.jpg" -->
## 10a. Lach - Komedie?
Ace Ventura<!-- .element: class="answer" -->
Note: Jim Carrey speelt in deze film een privédetective gespecialiseerd in het opsporen van vermiste dieren. Op een dag is de mascotte van het American football team de Miami Dolphins ontvoerd. Aan het eind van de film vindt Jim Carrey de mascotte net op tijd terug, zodat de mascotte aanwezig kan zijn bij de Super Bowl. Wat is de naam van deze komedie?

---
<!-- .slide: data-background="img/10b.jpg" -->
## 10b. Traan - Tragedie?
Eternal sunshine of the spotless mind<!-- .element: class="answer" -->
Note: Tegenpolen Joel Barish (gespeeld door Jim Carrey) en Clementine Kruczynski (gespeeld door Kate Winslet) vielen twee jaar geleden als een blok voor elkaar, maar de tegenstellingen in hun karakters worden na verloop van tijd te groot. Na weer een ruzie, wil Joel het de volgende dag goed gaan maken. Clementine blijkt alleen totaal niet meer te weten wie hij is. Joel komt er vervolgens achter dat Clementine een behandeling heeft ondergaan bij het bedrijf Lacuna, Inc., dat op haar verzoek al haar herinneringen aan hem heeft gewist. Daarop besluit hij om zelf eenzelfde behandeling te ondergaan. In zijn slaap beseffend wat er gebeurt, krijgt hij spijt en probeert hij het verwijderproces te stoppen om zo te voorkomen dat al zijn herinneringen aan Clementine in het niets verdwijnen. Graag de naam van deze tragedie?

---
<!-- .slide: data-background="img/afgeven.jpg" -->
## Afgeven!
x<!-- .element: class="answer" -->
<audio controls src="audio/end.mp3"></audio>
Note: This will only appear in the speaker notes window.
