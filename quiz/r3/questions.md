<!-- .slide: data-background="img/00.jpg" -->
## Nubera Tech Familie Quiz
### Binnen zonder bellen!

---
<!-- .slide: data-background="img/01.jpg" -->
## 1. Adelijke BV?
Marlène De WAUTERS <!-- .element: class="answer" -->
Note: Deze gewezen VTM presentatrische, met adelijk bloed, werd nooit Miss Belgie verkozen. Ze werd in Amsterdam geboren op 18 feb 1963 en was de eerste Belgische speelster die de WTA-ranking haalde. In 2007 schitterde ze nog in sterren op de dansvloer aan de zijde van Arend Vandewalle en werd uiteindelijk derdes dat seizoen. Welke persoonlijkheid zoeken we?

---
<!-- .slide: data-background="img/02.png" -->
## 2. Stad?
Dakar (Paris-Dakar) <!-- .element: class="answer" -->
Note: De Rode Duivels vertrokken in juni richting Brazilië voor het WK voetbal 2014. Ze moesten echter eerst nog een tussenlanding maken, alvorens de Atlantische Oceaan over te steken. In welke Afrikaanse stad, hoofdstad van een West-Afrikaans land en geboorteplaats van o.a. de Franse politica Ségolène Royal en voormalig voetballer Patrick Vieira, zetten ze nog voet aan de grond?

---
<!-- .slide: data-background="img/03.jpg" -->
## 3. Logo van welke organisatie?
Plan International (Peterschap) <!-- .element: class="answer" -->
Note: Deze internationale organisatie vooral bekend voor het financieel laten adopteren van kinderen in de derde wereld is onder andere actief in Togo, waar het vooral inzet op de bestrijding van kinderhandel. Hoe heet deze organisatie waarvan Philippe Gilbert in België één van de bekendste ambassadeurs is.

---
<!-- .slide: data-background="img/04.jpg" -->
## 4. Wielerterm?
EN DANSeuse (Lied van Clouseau)<!-- .element: class="answer" -->
Note: Voor de trouwe kijkers van Michel en José wordt vraag 3 een eitje. Als ze het niet hebben over een derailleur, chasse patate of musette dan misschien wel over deze koersterm, afkomstig uit het Frans. De term wordt gebruikt wanneer een renner op de trappers gaat lopen zonder op het zadel te zitten en zich al heen en weer wiegend een berg op trekt. Wat is deze tweeledige Franse term?

---
<!-- .slide: data-background="img/05.jpg" -->
## 5. Diamant?
The Pink Panther (Verwijst naar inspecteur Clouseau) <!-- .element: class="answer" -->
Note: Deze diamant, die ontgonnen wordt in het westen van Australië, hoort tot de duurste ter wereld. Hij was de inspiratie voor een film uit 1963, gevolgd door tien sequels, waarin jacht gemaakt wordt op de juwelendief Phantom. Hoe heet die diamant?

---
<!-- .slide: data-background="img/06.jpg" -->
## 6. De naam van dit strip personage?
De Rode ZITA (Dochter van...)<!-- .element: class="answer" -->
Note: Dat deze stripreeks over de historische bendeleider Ludovicus Baeckelandt gaat, ziet u uiteraard. Weet u ook de naam van zijn vriendin in de stripreeks? Het is tevens de titel van een van de verschenen strips.

---
<!-- .slide: data-background="img/07.jpg" -->
## 7. Gemeenschappelijkheid?
NOBELPRIJS (Lied van...)<!-- .element: class="answer" -->
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/08.jpg" -->
## 8. Woordmerk ?
K-Way (Initialen) <!-- .element: class="answer" -->
Note: Merk van regenkledij dat groot werd dankzij een uitvinding die Léon-Claude Duhamel in 1965 deed: een (min of meer) wind- en regendicht nylon jasje die tot een soort van heuptasje kon worden opgevouwen.

---
<!-- .slide: data-background="img/09.jpg" -->
## 9. Naam van de film?
Domino (Lied van...)<!-- .element: class="answer" -->
Note: In welke film van Tony Scott uit 2005 speelt Keira Knightley de rol van een bountyhunter, of anders gezegd een premiejaagster? De film is een biopic en vertelt het levensverhaal van een premiejaagster die in 2005 op 35-jarige leeftijd overleed ten gevolge van een overdosis pijnstillers. Andere opvallende namen in de cast zijn : Mickey Rourke, Edgar Ramirez, Macy Gray en Lucy Liu. Welke film zoeken we?

---
<!-- .slide: data-background="img/10.jpg" -->
## 10. LINK?
KOEN WAUTERS <!-- .element: class="answer" -->
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/afgeven.jpg" -->
## Afgeven!
x<!-- .element: class="answer" -->
<audio controls src="audio/end.mp3"></audio>
Note: This will only appear in the speaker notes window.
