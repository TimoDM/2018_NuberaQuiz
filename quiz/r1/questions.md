<!-- .slide: data-background="img/start.jpg" -->
## Nubera Team Quiz
### Opwarmeuh!

---
<!-- .slide: data-background="img/01.jpg" -->
## 1. Welk sponscakeje?
Madeleine <!-- .element: class="answer" -->
Note: Een fijn voorproevertje, we gaan met deze lichte hap starten. Franse gastronomie, inderdaad, iets om van te smikkellen. Welk Sint-Jacobsschelpvormig sponscakeje uit Lotharingen zien we hier?

---
<!-- .slide: data-background="img/02.jpg" -->
## 2. De naam van deze keizer?
Nero <!-- .element: class="answer" -->
Note: Deze Romeinse keizer werd geboren als Lucius Domitius Ahenobarbus. Na zijn adoptie door de toenmalige keizer Claudius veranderde zijn naam naar de naam waaronder hij bekend is geworden. Onder de eerste jaren van zijn bewind kende het Romeinse rijk een gouden periode. Daarna ging het enkel bergaf met een zelfmoord in de senaat tot gevolg. Zijn laatste woorden waren "Qualis artifex pereo" oftewel "Welk een kunstenaar sterft er met mij". Hij liet het Rijk bankroet en in totale chaos achter. De senaat vervloekte zijn nagedachtenis met de damnatio memoriae. Wat is de naam van deze keizer?

---
<!-- .slide: data-background="img/03.jpg" -->
## 3. De naam van deze groep?
Balthazar <!-- .element: class="answer" -->
<audio controls src="audio/03.mp3"></audio>
Note: Wat is de naam van deze Kortrijkse groep die we hier aan het werk horen?

---
<!-- .slide: data-background="img/04.jpg" -->
## 4. De naam van deze opera?
Carmen <!-- .element: class="answer" -->
Note: Ohhh oh oh oh oh oh oh oooooh..... Opera... Donderdagavond, datenight. Vanavond gaan we naar de opera. We kijken naar het tragisch liefdesverhaal van een Spaanse zigeunerin. Het verhaal is een bewerking van een roman van Prosper Mérimée, maar is eigenlijk vooral gekend als het schokkende meesterwerk van de hand van Georges Bizet. Graag de naam van de opera?

---
<!-- .slide: data-background="img/05.jpg" -->
## 5. De naam van deze man?
Pascal Smet <!-- .element: class="answer" -->
Note: Politiek, Brussel, SP.A., voormalig minister van Mobiliteit en Openbare Werken in de Brusselse regering, graag zijn voor- en familienaam!

---
<!-- .slide: data-background="img/06.jpg" -->
## 6. De naam van deze zanger?
Billy Joel <!-- .element: class="answer" -->
<audio controls src="audio/06.mp3"></audio>
Note: -

---
<!-- .slide: data-background="img/07.jpg" -->
## 7. De naam van deze man?
Xavier Taveirne <!-- .element: class="answer" -->
Note: Velen onder ons herkenden deze warme West-Vlaming van zijn zachte nieuwsstem op Radio 1 en Studio Brussel, maar sinds zijn succesvolle deelname aan de "Slimste mens ter wereld" van afgelopen jaar, kennen we ook zijn gezicht. Graag de voor- en familienaam van deze nieuwslezer.

---
<!-- .slide: data-background="img/08.jpg" -->
## 8. Eenheidsmaat?
Pico <!-- .element: class="answer" -->
Note: Voor de wiskundigen onder ons. Vervolledig volgende rij: Tera, giga, mega, kilo, milli, micro, nano, ... , femto, atto

---
<!-- .slide: data-background="img/09.jpg" -->
## 9. De naam van deze man?
Pol Pot <!-- .element: class="answer" -->
Note: De persoon die we zoeken werd leider van Cambodja op 17 april 1975. Tijdens zijn heerschappij dwong hij stadsbewoners te verhuizen naar het platteland om op collectieve boerderijen te werken en dwangarbeid te verrichten. De gecombineerde effecten van dwangarbeid, ondervoeding, slechte medische zorg en executies resulteerden in de dood van ongeveer 21% van de Cambodjaanse bevolking. In totaal zijn naar schatting 2 tot 4 miljoen mensen gestorven onder zijn leiderschap. Wie is deze tiran?

---
<!-- .slide: data-background="img/10.jpg" -->
## 10. De (afkortings-) naam van deze insecticide?
DDT <!-- .element: class="answer" -->
Note: De stof die we zoeken is een organische, chemische verbinding. Ze heeft een toxische werking voor luizen, bedwantsen, vlooien en muggen. De uitvinder, Paul Muller, werd hiervoor nog in 1948 met de Nobelprijs beloond, maar in 1962 ontdekte men tijdens wetenschappelijk onderzoek welke fallikante effecten de stof op het milieu heeft. We geven u ook graag nog de Chemische Formule C14H9Cl5, maar eigenlijk kennen we dit product voornamelijk onder zijn drieletter acroniem. Graag deze afkorting van de insecticide.

---
<!-- .slide: data-background="img/11.jpg" -->
## 11. Welke prijs?
Oscar <!-- .element: class="answer" -->
Note: Tijd voor wat glitter en glamour. We zoeken de bij het publiek meest gekende naam voor deze award. De prijs staat hoog aangeschreven en bezorgt de winnaars wereldwijde faam en eeuwige roem. De uitreiking vindt elk jaar plaats in de maand februari in het Dolby Theatre in Hollywood. Graag de naam voor deze felbegeerde prijs?

---
<!-- .slide: data-background="img/12.jpg" -->
## 12. Roepnaam van deze voetballer?
Ronaldinho <!-- .element: class="answer" -->
Note: Een ware kunstenaar op het gras; prominente passages bij PSG, Barcelona en AC Milan; titels, Champions League en wereldkampioen. Wat is de roepnaam van deze voetballer?

---
<!-- .slide: data-background="img/afgeven.jpg" -->
## Afgeven!
We are the Champions - Queen <!-- .element: class="answer" -->
<audio controls src="audio/end.mp3"></audio>
Note: This will only appear in the speaker notes window.
