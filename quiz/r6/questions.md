<!-- .slide: data-background="img/start.jpg" -->
## Nubera Tech Familie Quiz
### Nog steeds geen vraag over...

---
<!-- .slide: data-background="img/01.jpg" -->
## 1. De naam van dit bedrijf?
Taco Bell <!-- .element: class="answer" -->
Note: Olivia en Louwie zijn op citytrip in Amerika. Ze hebben beiden een "hongerken" bij hun roadtrip doorheen Texas. Op een geven moment zien ze een Drive True van een fastfoodketen die allerlei Tex-Mex specialiteiten verkoopt. Taco's, burrito's en nacho's. Welke keten zal hoogstwaarschijnlijk hun honger stillen?
---
<!-- .slide: data-background="img/02.jpg" -->
## 2. De naam van deze plant?
Berenklauw <!-- .element: class="answer" -->
Note: Bij het joggen in het bos, loopt Judith tegen deze plant met haar blote bovenarm. Ookal is deze plant niet giftig, Judith weet als huisarts dat ze best voorzichtig is met deze tweejarige plant een schermbloemplant is en dat deze fototoxisch is. Waar is Judith tegen gelopen?

---
<!-- .slide: data-background="img/03.jpg" -->
## 3. Klassieker?
Steak Tartaar<!-- .element: class="answer" -->
Note: De Dagschotel die nu en dan eens op een warme zomerdag op het menu staat in de Withoeve staat hier afgebeeld als individuele ingredienten. Tot wat tovert Kobe deze ingredienten om?

---
<!-- .slide: data-background="img/04.jpg" -->
## 4. Serie?
Lucifer<!-- .element: class="answer" -->
Note: Eindelijk, Zoë ligt slapend in haar bedje. Het is nog wat te vroeg om zelf te gaan slapen, dus beslist Waldek om nog even een serietje op te zetten. De serie loopt reeds sinds 2016 op de zender Fox. Het verhaal beschrijft de avonturen van de duivel die zich als persoon komt amuseren op de aarde. Hij had echter nooit verwacht dat het aardse leven zo complex kon zijn. Hij maakt vrienden en vijanden, leert omgaan met kinderen en wordt zelfs verliefd op een politieagente. Wat is de naam van het hoofdpersonage, tevens de naam van deze serie?

---
<!-- .slide: data-background="img/05.jpg" -->
## 5. Film?
The Last Samurai<!-- .element: class="answer" -->
Note: Movienight ten huize Cremers. Tim is even alleen thuis en denkt nog eens een DVDtje op te zetten uit zijn jonge jaren, waarin Tom Cruise zijn plek leert kennen en als Amerikaans legerkapitein gevangen wordt genoemen in Japan. Hij besluit de samenleving te omarmen en mee te vechten aan hun zijde. Welk DVD doosje lag er nu alweer op de salontafel van Tim?

---
<!-- .slide: data-background="img/06.jpg" -->
## 6. Modefenomeen?
Bikini bridge<!-- .element: class="answer" -->
Note: Ik ga op reis en ik neem mee... "Een strandbal, een strand laken en een bikini, meer hoef ik niet..." denkt Tamara, die vlug nog even haar uitstekende heupbeenderen checkt. Tamara is een hipster en zag laatst een modetrend om te showen in bikini hoe slank je bent... maar kennen jullie ook de naam van deze hype, waarbij het bikinibroekje zodanig opgespannen is zodat je de glooiing van de onderbuik mooi kan volgen.

---
<!-- .slide: data-background="img/07.jpg" -->
## 7. Wielrenner?
Iban Mayo<!-- .element: class="answer" -->
Note: Tijdens het aanleggen van een nieuwe tuin, hoort opper-kabouter Eddy het live verslag van de Tour de France beklimming richting Alpe D'Huez. Op de radio horen we Kristof Vandegoor en Frank Hoste gedetailleerde commentaar geven over deze etappe, waarbij ze geregeld ook een referentie maken naar de man die in 2003 als eerste boven kwam. Het betrof een Baskische Spanjaard van Euskatel. De renner maakte in 2002 al furore door 5e te worden in de Vuelta en is misschien bekend van de snelste beklimming ooit van den Mont Ventoux, wie zoeken we?

---
<!-- .slide: data-background="img/08.jpg" -->
## 8. Dier?
Grizzly beer<!-- .element: class="answer" -->
Note: Terug naar Louwie en Olivia die intussen in het bosrijke Noord-Westen van Amerika zijn aangekomen. Ze zitten in een klein bootje op een stroompje, waar ze nu en dan enkele borden tegenkomen die vermelden "Deze Ursus Arctos Horribilis zijn gevaarlijk!". Smartass Louwie probeert wat te pochen met zn kennis dat het een bruine beer betreft, maar wordt al gauw gecorrigeerd door Olivia die wel de correcte ondersoort kent. Hoe heet deze beer?

---
<!-- .slide: data-background="img/09.jpg" -->
## 9. Streek?
Andalusië<!-- .element: class="answer" -->
Note: Dokter Ann is het griepseizoen meer dan beu en denkt er aan om er even een weekje tussenuit te knijpen. Ze boekt via TuiFly een ticket richting Malaga. Maar in welke Spaanse provincie ligt deze stad?

---
<!-- .slide: data-background="img/10.jpg" -->
## 10. Gerecht?
Shashlik<!-- .element: class="answer" -->
Note: Eindelijk tijd om een frietje te steken op vrijdagavond. Dieter en Nancy willen Brittanieke eens verwennen met een Smulbox, Nancy wil Bicky Cheese bij haar frietjes en Dieter wil graag een gerecht dat een Russische naam heeft. We kennen het vaak onder de Turkse betiteling, welke "Shish Kebab" is. Maar hoe heet de Russische variant van de reuzebrochette?

---
<!-- .slide: data-background="img/11.jpg" -->
## 11. Etnische achtergrond?
Zigeuneres<!-- .element: class="answer" -->
Note: Na hun frietjes, willen Dieter en Nancy nog even wat Quality Time en zetten Britney voor de televisie met een klassieke Disney-film.. "De klokkenluider van de Notre Dame", waarin bultenaar Quasimodo het hart probeert te veroveren van de schone Esmeralda. Maar wat was de etnische oorsprong van Esmeralda?

---
<!-- .slide: data-background="img/12.jpg" -->
## 12. Lied?
Despacito<!-- .element: class="answer" -->
Note: This will only appear in the speaker notes window.

---
<!-- .slide: data-background="img/afgeven.jpg" -->
## Afgeven!
x<!-- .element: class="answer" -->
<audio controls src="audio/end.mp3"></audio>
Note: This will only appear in the speaker notes window.
