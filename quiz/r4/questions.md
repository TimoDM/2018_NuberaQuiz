<!-- .slide: data-background="img/start.jpg" -->
## Nubera Tech Familie Quiz
### Wie ben ik...?

---
<!-- .slide: data-background="img/01.jpg" -->
## 1. Sportjournalist?
Aster Nzeyimana<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/02.jpg" -->
## 2. Atlete
Elodie Ouedraogo<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/03.jpg" -->
## 3. TV-kok
Jeroen Depauw<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/04.jpg" -->
## 4. Celeb
Rihanna<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/05.jpg" -->
## 5. Politica
Joke Schauvliege<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/06.jpg" -->
## 6. Actrice
Lucy Liu<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/07.jpg" -->
## 7. Internationale Politieker
Ban Ki Moon<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/08.jpg" -->
## 8. Acteur
Jackie Chan<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/09.jpg" -->
## 9. Cartoonpersonage
Ralph Wiggum<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/10.jpg" -->
## 10. Zangers
Florence Welsch<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/11.jpg" -->
## 11. Comedienne
Lies Lefever<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/12.jpg" -->
## 12. Model / Celeb
Ivanka Trump<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/13.jpg" -->
## 13. Muzikant
Jef Neve<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/14.jpg" -->
## 14. Internationale Politieker
Koffi Annan<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/15.jpg" -->
## 15. Atlete
Hanna Marien<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/16.jpg" -->
## 16. Grootheid
Michael Jordan<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/17.jpg" -->
## 17. TV-kokin
Dagny Ros Asmundsdottir <!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/18.jpg" -->
## 18. Sportheld
Frank Vandenbroucke<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/19.jpg" -->
## 19. Kunstenaar
Andy Warhol<!-- .element: class="answer" -->

---
<!-- .slide: data-background="img/20.jpg" -->
## 20. Wetenschapster
Marie Curie<!-- .element: class="answer" -->
