<!-- .slide: data-background="img/start.png" -->
## Nubera Tech Familie Quiz
### ABC ronde!

---
<!-- .slide: data-background="img/01.jpg" -->
## 1. Music maestro?
Qmusic <!-- .element: class="answer" -->
Note: We zoeken de naam van een Belgisch, commercieel radiostation van mediabedrijf Medialaan. Anno 2016 is deze zender de grootste commerciële radiozender en de derde grootste radiozender van Vlaanderen. Volgens de meest recente luistercijfers van het Centrum voor Informatie over de Media behaalt de zender op dagbasis een gemiddeld marktaandeel van 11,08%.

---
<!-- .slide: data-background="img/02.jpg" -->
## 2. Ras hond?
Chow Chow <!-- .element: class="answer" -->
Note: Woef woef, grom grom, ... Dit hondenras behoort tot de Aziatische- en keeshonden van het oertype. Nu is hij vooral in gebruik als huis- en waakhond, maar ooit werd hij gebruikt als jachthond, en later zelfs als voedsel voor de mens. Het is een rustige, zelfverzekerde hond, moedig en waaks. Kenmerkend is hun blauwe tong en tandvlees. Wat is de naam van dit hondenras?

---
<!-- .slide: data-background="img/03.jpg" -->
## 3. Animatiefiguur?
Winnie the Pooh <!-- .element: class="answer" -->
Note: Jeugdnostalgie. We zoeken een personage uit een verhalenreeks van A.A. Milne. Zijn naam is eigenlijk Beer Edward, maar hij wordt zelden zo genoemd. Deze vriendelijke beer is dol op honing en heeft naar eigen zeggen maar een heel klein beetje verstand. Kenmerkend is zijn geelbruine vacht en rode trui. Wat is de volledige naam van deze beer?

---
<!-- .slide: data-background="img/04.jpg" -->
## 4. Stripfiguur?
Haddock <!-- .element: class="answer" -->
Note: We zoeken een aan alcohol verslaafd personage uit een reeks stripverhalen. Later in de reeks krijgt hij steeds meer vat op zijn drankprobleem, zodat hij zelfs benoemd wordt tot erevoorzitter van de LZG, de liga der zeevarende geheelonthouders. Verder kan hij minutenlang schelden zonder in herhaling te vallen. Weinig bekend is zijn voornaam Archibald, maar wat is zijn achternaam?

---
<!-- .slide: data-background="img/05.jpg" -->
## 5. Zangeres?
Katy Perry <!-- .element: class="answer" -->
<audio controls src="audio/05.mp3"></audio>
Note: Muziekfragment, graag de voor en achternaam van deze Amerikaanse zangeres.

---
<!-- .slide: data-background="img/06.jpg" -->
## 6. Roepnaam?
Yankees <!-- .element: class="answer" -->
Note: Tijd voor sport, meerbepaald baseball. We vragen de roepnaam van deze honkbalploeg die zijn thuishaven vindt in de bronx. Het team is de grootse aartsrivaal van de Boston Red Sox en heeft met Joe Dimaggio de bekendste honkballer aller tijden in zijn rangen gehad. Deze ploeg is ook de favoriete ploeg van Peter Burke in de serie White Collar.

---
<!-- .slide: data-background="img/07.jpg" -->
## 7. Hoofdstad?
Sarajevo <!-- .element: class="answer" -->
Note: Het zat de Oostenrijkse troonopvolger, die we hier moedwillig niet bij naam noemen, toch ook niet mee. Tijdens een werkbezoek aan de hoofdstad van de Oostenrijk-Hongaarse provincie Bosnië-Herzegovina werd hij samen met zijn vrouw Sophie Chotek doodgeschoten door een aanhanger van de Zwarte Hand, een Servisch Nationalistische beweging. Wat is de naam van de toenmalige en huidige hoofdstad van Bosnië-Herzegovina?

---
<!-- .slide: data-background="img/08.jpg" -->
## 8. Personage (voor- en familienaam)?
Oliver Twist <!-- .element: class="answer" -->
Note: De Britse schrijver Charles Dickens kon op zijn beurt niet leven met de wantoestanden in de Victoriaanse samenleving. Kinderarbeid in de zogenaamde “work houses” en de onderwereld waarin straatkinderen tot crimineel worden opgeleid, spelen dan ook een grote rol in zijn roman die werd uitgebracht in 1838. De hoofdpersoon, waarnaar het boek tevens genoemd is, is een weesjongen door wiens ogen het harde leven in Londen wordt bekeken. Over welke roman hebben we het?

---
<!-- .slide: data-background="img/09.jpg" -->
## 9. Keukentijd?
Tijm <!-- .element: class="answer" -->
Note: We zoeken een kruid met een erg aromatische smaak. Het kruid kan tot 30cm hoog worden en bloeit in schijnkransen. De planten komen algemeen voor in het Middellandse-Zeegebied en in Azië, maar een drietal soorten komt ook voor in onze streken. Vanwege zijn aromatische geur wordt de plant dikwijls gebruikt in de zeepindustrie. Ze worden ook ingezet als genneskrachtige planten en als sierplanten. Bloemen en bladeren worden gebruikt om siroop en thee mee te bereiden. Het zoetige maar sterke aroma is uitstekend geschikt voor het kruiden van groente, vis, vlees en sauzen. Het wordt ook gebruikt bij de bereiding van likeuren zoals Bénédictine. Graag de naam van dit kruid.

---
<!-- .slide: data-background="img/10.jpg" -->
## 10. Berg?
Matterhorn <!-- .element: class="answer" -->
Note: De chocoladeliefhebbers onder ons zullen allicht weten dat op de verpakkingen van het Zwitserse merk Toblerone een berg staat afgebeeld, met – als u goed kijkt – daarin zelfs een beer, die naar de stad Bern verwijst. Deze berg ligt op de grens tussen Zwitserland en Italië en is met zijn iconische piramidevorm het symbool geworden voor Zwitserland. Het is niet de hoogste berg van het land, maar met zijn 4478m wel een stevige klepper. Welke berg is dit?

---
<!-- .slide: data-background="img/11.jpg" -->
## 11. Volledige naam?
Noord-Atlantische Verdragsorganisatie<!-- .element: class="answer" -->
Note: Oef, een vraag met minder praten. Van welke organisatie is dit het logo? Graag de volledige naam, geen afkorting.

---
<!-- .slide: data-background="img/12.jpg" -->
## 12. Larve?
Engerling<!-- .element: class="answer" -->
Note: Hoe heten deze kleine uitziende monstertjes? Het zijn larven kevers die tot de bladsprietkevers behoren, zoals bijvoorbeeld een meikever.

---
<!-- .slide: data-background="img/13.jpg" -->
## 13. Staatsman?
Gorbatsjov<!-- .element: class="answer" -->
Note: Deze man was van 1985 Secretaris-Generaal van de Communistische Partij van de Sovjet Unie, en schopte het in 1990 tot president van de Sovjet Unie. Hij kwam aan de macht in een periode waar de Sovjet Unie een grote malaise meemaakte. Onder andere door zijn glasnost en perestroika trachtte hij de meubelen nog te redden. Die plannen werkten aanvankelijk niet slecht, maar al snel bleek dat leiders uit deelstaten, onder invloed van nationalistische stromingen, hem te veel tegenwerkten om van een totale remonte te kunnen spreken. Uiteindelijk werd de Sovjet Unie kort daarna ontbonden en betekende dat ook het einde van de Koude Oorlog. Wat is de naam van deze man die voor zijn inspanningen toch nog de Nobelprijs voor de Vrede kreeg in 1990?

---
<!-- .slide: data-background="img/14.jpg" -->
## 14. Architect?
Victor Horta<!-- .element: class="answer" -->
Note: De architect die we zoeken werd geboren in Gent in 1861 en was samen met Henry Van de Velde een van de belangrijkste vertegenwoordigers van de Art Noveau. Rond 1885 had hij een eigen praktijk en zijn eerste realisaties waren enkele woonhuizen in Gent. Een viertal van zijn burgerwoningen in Brussel, waarvan we er een zien op de foto, behoren tot het UNESCO Werelderfgoed. Wie is deze architect?

---
<!-- .slide: data-background="img/15.jpg" -->
## 15. Muziekgroep?
Arsenal<!-- .element: class="answer" -->
<audio controls src="audio/15.mp3"></audio>
Note: Welke belgische muziekgroep horen we hier aan het werk? Als extra tip staat op de foto een gelijknamig voetbalteam afgebeeld.

---
<!-- .slide: data-background="img/16.jpg" -->
## 16. Dier?
Lynx<!-- .element: class="answer" -->
Note: Deze majestueuze katachtige is een van de meest voorkomende katachtigen. Het schuchtere roofdier jaagt vooral in de ochtend en bij het vallen van de avond. Er bestaan nog slechts vier soorten in de wereld: de Canadese, de rode, de Iberische en de Euraziatische. Ze worden ongeveer 130 cm groot en kunnen tot 28 kg wegen. Hij is gemakkelijk te herkennen aan zijn gevlekte vacht, zijn zwarte ‘gepluimde’ oren en zijn karakteristieke bakkebaarden. Wat is de naam van deze katachtige?

---
<!-- .slide: data-background="img/17.jpg" -->
## 17. Term?
Xenofoob<!-- .element: class="answer" -->
Note: Hoe noemt men in 1 woord iemand die een sterke afkeer van vreemdelingen heeft en vaak ook haatdragend is tegenover buitenlanders of gewoontes uit andere culturen? Het woord is een samentrekking van de twee Oudgriekse woorden voor ‘vreemdeling’ en ‘angst’.

---
<!-- .slide: data-background="img/18.jpg" -->
## 18. Zanger?
Boef<!-- .element: class="answer" -->
Note: De rapper in kwestie, is vandaag de dag niet zo graag meer gezien onder de Belgen. Zo kwam hij laatst op zijn concert in de Bocca in Destelbergen een uur te laat aan, kreeg al gauw problemen met zijn microfoon en vluchtte huiswaards na amper 15 minuten van zijn beoogde 4uur durende concert. Hoe heet deze gluiperd?!

---
<!-- .slide: data-background="img/19.jpg" -->
## 19. Historich figuur?
Franz<!-- .element: class="answer" -->
Note: Daarnet is hij al ter sprake gekomen, de Oostenrijkse troonopvolger. Nu vragen we zijn voornaam. Als tip kunnen we nog meegeven dat zijn volledige naam gelijk is als die van een Schotse muziekgroep, zijn voornaam is ook de naam van een mac applicatie gebruikt binnen Nubera, geïntroduceerd door onze geniale collega Timo.

---
<!-- .slide: data-background="img/20.jpg" -->
## 20. Film, alfabet?
Zulu<!-- .element: class="answer" -->
Note: De film die we zoeken uit 1964 is gebaseerd op een historische gebeurtenis, tijdens een oorlog in 1879 op het Afrikaanse continent die dezelfde naam droeg. Een honderdtal Britse soldaten verdedigen een post tegen een aanvalsmacht van 4000 Afrikaanse krijgers nadat eerder die dag een meer dan tien keer grotere Engelse legermacht bij Isandlwana onder de voet werd gelopen door 20.000 man. Wat is de titel van deze bij tijden licht racistische film?. Extra tip: Navo alfabet.

---
<!-- .slide: data-background="img/21.jpg" -->
## 21. Film?
Underworld<!-- .element: class="answer" -->
Note: Op 15 oktober 2003 kwam in ons land een horrorfilm uit die intussen tot een franchise verworden is, want in 2016 komt deel 5 al in de zalen. Kate Beckinsale speelt in alle films de rol van Selene, een 'Death Dealer' die zich specialiseert in het doden van Lycans ofwel weerwolven. Enkele titels in de franchise: Evolution, Rise of the Lycans en Awakening. Welke franchise?

---
<!-- .slide: data-background="img/22.jpg" -->
## 22. Persoon?
Dansercoer<!-- .element: class="answer" -->
Note: Deze Vlaming heeft een haat-liefdeverhouding met de polen en dan heb ik het niet over de inwoners van het gelijknamige land. In 1997-1998 maakte hij met zijn reisgenoot in 99 dagen de oversteek van Antarctica over een afstand van bijna 4.000 km. In 2002 trachtten zij samen in 100 dagen de Noordpool over te steken maar na 30 dagen moesten ze de tocht staken omdat ze op dat moment nog maar 10% van de 2.400 km hadden afgelegd en er slechts voorraad was voor 100 dagen. Wie is deze Nieuwpoortse poolreiziger die heel wat boeken over zijn reizen op zijn conto heeft staan en die in tegenstelling tot zijn metgezel niet in een slecht daglicht werd geplaatst?

---
<!-- .slide: data-background="img/23.jpg" -->
## 23. Drank?
Raki<!-- .element: class="answer" -->
Note: We vragen de naam van een Turkse alcoholische drank, die meestal met anijs gekruid is en een alcoholpercentage heeft van ongeveer 45%. De drank wordt veelal met water aangelengd. De oorspronkelijk heldere kleur wordt dan, indien er in de variant anijs zit, troebelwit.

---
<!-- .slide: data-background="img/24.jpg" -->
## 24. Wetenschap?
Isotoop<!-- .element: class="answer" -->
Note: Atomen van hetzelfde chemische element kunnen soms een verschillend aantal protonen hebben in hun kern. Het is 2 keer hetzelfde element, alleen met een meer of minder stabiele kern en een verschillende atoommassa. Hoe noemen we zulke atomen van hetzelfde element met hetzelfde aantal protonen, maar met een verschillend aantal neutronen? Tip: het baseballteam van Springfield in The Simpsons is er naar genoemd.

---
<!-- .slide: data-background="img/25.jpg" -->
## 25. Kledingstuk?
Pij<!-- .element: class="answer" -->
Note: We zoeken de naam van het traditionele kledingstuk gedragen door monniken en paters.

---
<!-- .slide: data-background="img/26.jpg" -->
## 26. Uitvoerder?
John Newman<!-- .element: class="answer" -->
<audio controls src="audio/26.mp3"></audio>
Note: Graag de voor en achternaam van de uitvoerder van deze hit.

---
<!-- .slide: data-background="img/afgeven.jpg" -->
## Afgeven!
x<!-- .element: class="answer" -->
<audio controls src="audio/end.mp3"></audio>
Note: This will only appear in the speaker notes window.
