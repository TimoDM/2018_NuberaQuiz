/**
 * Handles opening of and synchronization with the reveal.js
 * notes window.
 *
 * Handshake process:
 * 1. This window posts 'connect' to notes window
 *    - Includes URL of presentation to show
 * 2. Notes window responds with 'connected' when it is available
 * 3. This window proceeds to send the current presentation state
 *    to the notes window
 */
var Answers = (function() {

	if( !/receiver/i.test( window.location.search ) ) {

		// Open the notes when the 'h' key is hit
		document.addEventListener( 'keydown', function( event ) {
			// Disregard the event if the target is editable or a
			// modifier is present
			if ( document.querySelector( ':focus' ) !== null || event.shiftKey || event.altKey || event.ctrlKey || event.metaKey ) return;

			// Disregard the event if keyboard is disabled
			if ( Reveal.getConfig().keyboard === false ) return;

			if( event.keyCode === 87 ) {
				event.preventDefault();
				
                elements = document.getElementsByClassName('answer');
                for (var i = 0; i < elements.length; i++) {
                    console.log(i);
                    elements[i].style.display = elements[i].style.display == 'inline' ? 'none' : 'inline';
                }
			}
		}, false );

		// Show our keyboard shortcut in the reveal.js help overlay
		if( window.Reveal ) Reveal.registerKeyboardShortcut( 'W', 'Show Answers' );

	}

	return null;

})();
